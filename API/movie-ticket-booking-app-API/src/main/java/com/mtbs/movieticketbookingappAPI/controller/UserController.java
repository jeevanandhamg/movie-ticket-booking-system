package com.mtbs.movieticketbookingappAPI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mtbs.movieticketbookingappAPI.model.UserEntity;
import com.mtbs.movieticketbookingappAPI.model.UserModel;
import com.mtbs.movieticketbookingappAPI.service.UserService;


@RequestMapping(value = "mtbs/api/1.0/")
@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@CrossOrigin("http://localhost:4200/")
	@PostMapping("/checkUser")
	@ResponseBody
	public boolean checkUser(@RequestBody UserEntity user) {
		
		return this.userService.checkUser(user);
	}
	
	@CrossOrigin("http://localhost:4200/")
	@PostMapping("/regUser")
	@ResponseBody
	public UserEntity RegisterUser(@RequestBody UserEntity user) {
		
		return this.userService.RegisterUser(user);
	}
}
