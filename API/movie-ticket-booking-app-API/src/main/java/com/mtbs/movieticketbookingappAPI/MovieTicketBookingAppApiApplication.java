package com.mtbs.movieticketbookingappAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
@EnableAutoConfiguration
public class MovieTicketBookingAppApiApplication {

	public static void main(String[] args) {
		System.out.println("Started");
		SpringApplication.run(MovieTicketBookingAppApiApplication.class, args);
		System.out.println("Started");
	}

}
