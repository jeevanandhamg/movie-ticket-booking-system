package com.mtbs.movieticketbookingappAPI.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mtbs.movieticketbookingappAPI.model.MovieEntity;

@Repository
public interface MovieRepository extends JpaRepository<MovieEntity, Long> {
//	@Query(value="select * from mtbs.users where user_name = ?1 and password=?2", nativeQuery=true)
//	List<UserEntity> checkUser(String uName, String pwd);
}
