package com.mtbs.movieticketbookingappAPI.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtbs.movieticketbookingappAPI.model.MovieEntity;
import com.mtbs.movieticketbookingappAPI.repository.MovieRepository;

@Service
public class MovieService {

	@Autowired
	private MovieRepository mRepo;
	
	public List<MovieEntity> getMovies(){
		List<MovieEntity> l3=this.mRepo.findAll();
		
		return l3;
	}
	
	public MovieEntity addMovie(MovieEntity movie){
		MovieEntity l3=this.mRepo.save(movie);
		return l3;
	}
}
