package com.mtbs.movieticketbookingappAPI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mtbs.movieticketbookingappAPI.model.MovieEntity;
import com.mtbs.movieticketbookingappAPI.service.MovieService;

@RequestMapping(value = "mtbs/api/1.0/")
@RestController
public class MovieController {
	@Autowired
	private MovieService mService;
	
	@CrossOrigin("http://localhost:4200/")
	@PostMapping("/getMovies")
	@ResponseBody
	public List<MovieEntity> getMovies() {
		
		return this.mService.getMovies();
	}
	
	@CrossOrigin("http://localhost:4200/")
	@PostMapping("/addMovie")
	@ResponseBody
	public MovieEntity addMovie(@RequestBody MovieEntity movie) {
		
		return this.mService.addMovie(movie);
	}
}
