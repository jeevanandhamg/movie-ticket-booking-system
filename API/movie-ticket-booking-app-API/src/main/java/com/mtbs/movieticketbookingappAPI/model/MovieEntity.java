package com.mtbs.movieticketbookingappAPI.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name = "movies")
public class MovieEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="movie_id")
	private Integer mId;
	
	@Column(name="movie_name")
	private String mName;
	
	@Column(name="movie_date")
	private Date mDate;
	
	@ColumnDefault("100")
	@Column(name="seats_available")
	private Integer seatsAvail;

	public Integer getmId() {
		return mId;
	}

	public void setmId(Integer mId) {
		this.mId = mId;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public Date getmDate() {
		return mDate;
	}

	public void setmDate(Date mDate) {
		this.mDate = mDate;
	}

	public Integer getSeatsAvail() {
		return seatsAvail;
	}

	public void setSeatsAvail(Integer seatsAvail) {
		this.seatsAvail = seatsAvail;
	}

}
